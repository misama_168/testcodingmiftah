<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		// check_valid_token('login');
        $this->load->helper('base_url_helper');
    }

	public function index()
	{
		$type = $_GET['type'] ?? 'created_at';
		$val = $_GET['val'] ?? 'desc';

		$data['data'] = $this->db->select('*')->where('stock != 0')->order_by($type, $val)->get('product')->result();
        $this->load->view('home',$data);
	}

	public function buy()
	{
		$post = $this->input->post();
		$stock = (int)$post['stock'];
		foreach ($post['quant'] as $x) {
			$qty = (int)$x;	
		}
		$sisa = $stock - $qty ;

		$id = $post['product_id'];
		$input['product_id'] = $id;
		$input['qty'] = $qty;

		$sold = $this->db->select('sum(qty) as terjual')->where('product_id',$id)->get('transactions')->result()[0];

		$data['stock'] = $sisa;
		if(empty($sold)){
			$data['sold'] = $qty;
		}else{
			$data['sold'] = (int)$sold->terjual + $qty;
		}

		$this->db->insert('transactions', $input);
		$this->db->where('id', $id)->update('product', $data);

		$result = array(
            "code" => 200,
            "msg" => 'Yap ! Pembelianmu berhasil'
        );
        echo json_encode($result);

	}

}
