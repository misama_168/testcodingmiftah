<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		// check_valid_token('login');
        $this->load->helper('base_url_helper');
        $this->load->library('template');
        $this->load->library('datatables');
    }

	public function index()
	{
        $data = [];
		$content = $this->load->view('admin/dashboard', $data, TRUE);
        $setting['footer'] 	= array('js' => 'custom.js');
		$this->template->load($setting, $content);
	}

    public function upload_image()
	{
        $data['error'] = '';
		$content = $this->load->view('admin/upload', $data, TRUE);
        $setting['footer'] 	= array('js' => 'custom.js');
		$this->template->load($setting, $content);
	}

    public function data_trx(){

        $condition = 'a.id is not null ';
        $type = @$_GET['type'] ? $_GET['type'] : 'created_at' ;

        if ( ! empty($_GET['start_date']) OR ! empty($_GET['end_date']))
		{
			if ( ! empty($_GET['start_date']) AND ! empty($_GET['end_date']))
			{
				$condition .= " AND ( date(a.created_at) BETWEEN '".date($_GET['start_date'])."' AND '".date($_GET['end_date'])."')";
			}
			elseif (!empty($_GET['start_date']))
			{
				$condition .= " AND ( date(a.created_at) BETWEEN >= '".date($_GET['start_date'])."')";
			}
			elseif ( ! empty($_GET['end_date']))
			{
				$condition .= " AND date(a.created_at) BETWEEN >= <= '".date($_GET['end_date'])."')";
			}
		}


        $this->datatables->select('a.id,a.*,b.name as produk');
		$this->datatables->from('transactions a');
        $this->datatables->join('product b', 'b.id = a.product_id');
        if($type != 'qtys'):
        $this->db->order_by('a.'.$type.'','desc');
        else:
        $this->db->order_by('a.qty','asc');
        endif;

        if($condition!=""): 
            $this->datatables->where($condition, NULL, FALSE);
        endif;
        echo $this->datatables->generate();
        exit;
    }

    public function list($type)
	{

        if($type == 'catalog'){
            $data['title'] = 'catalog';
            $data['menu'] = array(
                'No',
                'Name',
                'Action'
            );
        }else{
            $data['title'] = 'product';
            $data['menu'] = array(
                'No',
                'Catalog',
                'Name',
                'Stock',
                'Sold',
                'Action'
            );
            $data['catalog'] = $this->db->select('*')->get('catalog')->result();
        }

		$content = $this->load->view('admin/list', $data, TRUE);
        $setting['footer'] 	= array('js' => 'custom.js');
		$this->template->load($setting, $content);
	}


    public function data_list(){
        $type = $_GET['type'];
        if($type != 'catalog'){
            $this->datatables->select('a.id,a.*,b.names as catalog');
            $this->datatables->from('product a');
            $this->datatables->join('catalog b', 'b.id = a.catalog_id');
            $this->db->order_by('a.created_at','desc');
        }else{
            $this->datatables->select('a.*');
            $this->datatables->from('catalog a');

        }
   
        echo $this->datatables->generate();
        exit;
    }

    public function data_edit(){
        $id = $_GET['id'];
        $type = $_GET['type'];

        if($type != 'catalog'){
           $data = $this->db->select('*')->where('id',$id)->get('product')->result();
        }else{
          $data = $this->db->select('*')->where('id',$id)->get('catalog')->result();
        }

        $result = array(
            "code" => 200,
            "data" => $data[0],
            "type" => $type
        );
        echo json_encode($result);
    }

    public function action(){
        $post = $this->input->post();
        $type = $post['type'];
        $id = $post['id-'.$type];

        if($type != 'catalog') {
            $data['name'] = $post['name'] ;
            $data['stock'] = $post['stock'] ;
            $data['catalog_id'] = $post['catalog'] ;
            
            $config['upload_path'] = 'public/storage/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 204800;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
    
        }else{
            $x_data['names'] = $post['names'] ;
            
        }

        if(empty($id)){
            if($type != 'catalog') {

                if(!$this->upload->do_upload('product_image')){
                    $msg = array('error' => $this->upload->display_errors());
                    $code = 'ERR';

                }else{

                    $uploadData = $this->upload->data(); 
                    $filename = $uploadData['file_name']; 

                    $data['img'] = $filename;
                    $this->db->insert('product', $data);

                    $msg = 'Data berhasil ditambah';
                    $code = 'SS';
                }
              
                
            }else{

                $msg = 'Data berhasil ditambah';
                $code = 'SS';
                $this->db->insert('catalog', $x_data);
            }

        
            $res['n'] = $code;
            $res['m'] = $msg;
       
        }else{

            if($type != 'catalog') {
                $this->db->where('id', $id)->update('product', $data);
                $code = 'SS';
                $msg = 'Data berhasil dirubah';
            }else{
                $check = $this->db->where('catalog_id',$id)->get('product')->result();

                if(empty($check)){
                    $this->db->where('id', $id)->update('catalog', $x_data);
                    $code = 'SS';
                    $msg = 'Data berhasil dirubah';
                }else{
                    $code = 'ERR';
                    $msg = 'Data sedang terpakai oleh data lain';
                }
                
            }
          
            $res['n'] = $code;
            $res['m'] = $msg;
        
        }
        exit(json_encode($res));
  
    }


    public function delete_data(){
        $id = $_GET['id'];
        $type = $_GET['type'];

        if($type == 'catalog'):
        $check = $this->db->where('catalog_id',$id)->get('product')->result();
        if(empty($check)){
            $this->db->where('id', $id)->delete('catalog');
            $code = 200;
            $msg = 'Data berhasil dihapus';
        }else{
            $code = 500;
            $msg = 'Data sedang terpakai oleh data lain';
        }
        else:
            $this->db->where('id', $id)->delete('product');
            $code = 200;
            $msg = 'Data berhasil dihapus';
        endif;

        $result = array(
            "code" => $code,
            "msg" => $msg
        );
        echo json_encode($result);
    }


}
