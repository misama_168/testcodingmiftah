<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- Page Content-->
<div class="page-content">
        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col">
                                <h4 class="page-title">DATA TRANSAKSI</h4>
                            </div><!--end col-->
                        </div><!--end row-->                                                              
                    </div><!--end page-title-box-->
                </div><!--end col-->
            </div><!--end row-->
            <!-- end page title end breadcrumb -->

            <div class="row mt-2">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="filter">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="mb-1">Urutkan</label>
                                        <select class="form-control w-100" name="type" id="type">
                                            <option value="">Pilih</option>
                                            <option value="qty">Terjual Banyak</option>
                                            <option value="qtys">Terjual Sedikit</option>
                                            <option value="product_id">Produk</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                    <label class="mb-1">Start</label>
                                    <div class="col-md-12">                                 
                                        <input class="form-control w-100" id="daterange" type="text">
                                        <input type="hidden" id="start_date" name="start_date">
                                        <input type="hidden" id="end_date" name="end_date">
                                    </div>
                                </div>
                            
                                    <div class="col-md-2 row">
                                        <label class="mb-4"></label>
                                        <button type="submit" class="btn btn-info w-100"><i class="mdi mdi-filter"></i>CARI</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0" id="table-trx">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produk</th>
                                        <th>Qty</th>
                                        <th>Transaction Date</th>
                                        <th>Customer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table><!--end /table-->
                            </div><!--end /tableresponsive-->
                        </div><!--end card-body-->
                    </div><!--end card-->
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div><!-- container -->
    </div>
    <!-- end page content -->
</div>
<!-- end page-wrapper -->
