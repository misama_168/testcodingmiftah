<!-- Page Content-->
<div class="page-content">
        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col">
                                <h4 class="page-title">DATA <?=strtoupper($title)?></h4>
                            </div><!--end col-->
                            <div class="col-auto align-self-center">
                                <a class="btn btn-info" href="#" onclick="add('<?=$title?>')"><i data-feather="plus-circle"></i> ADD <?=strtoupper($title)?></a>
                            </div>
                        </div><!--end row-->                                                              
                    </div><!--end page-title-box-->
                </div><!--end col-->
            </div><!--end row-->
            <!-- end page title end breadcrumb -->

            <div class="row mt-2">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <input type="hidden" id="type" value="<?=$title?>">
                                <table class="table table-striped mb-0" id="table-<?=$title?>">
                                    <thead>
                                    <tr>
                                        <?php foreach($menu as $val):?>
                                        <th><?=$val?></th>
                                        <?php endforeach;?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table><!--end /table-->
                            </div><!--end /tableresponsive-->
                        </div><!--end card-body-->
                    </div><!--end card-->
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div><!-- container -->
    </div>
    <!-- end page content -->
</div>
<!-- end page-wrapper -->


<div class="modal fade" id="modal_<?=$title?>" tabindex="-1" role="dialog" aria-labelledby="modal_level" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title m-0" id="modal_header_<?=$title?>"></h6>
            </div><!--end modal-header-->
            <form id="action" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        <?php if($title != 'catalog'):?>
                            <div class="form-group mb-3 row">
                                <label for="job_title" class="col-sm-4 form-label align-self-center mb-lg-0 text-start">Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" value="" id="name" name="name">
                                </div>
                            </div>
                            <div class="form-group mb-3 row">
                                <label for="job_title" class="col-sm-4 form-label align-self-center mb-lg-0 text-start">Stock</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="number" max="100" value="" id="stock" name="stock">
                                </div>
                            </div>
                            <div class="form-group mb-3 row">
                            <label for="job_title" class="col-sm-4 form-label align-self-center mb-lg-0 text-start">Catalog</label>
                            <div class="col-sm-8">
                            <select class="form-control" name="catalog" id="catalog">
                                <option value="">Pilih</option>
                                <?php foreach($catalog as $row): ?>
                                    <option value="<?=$row->id?>"><?=$row->names?></option>
                                <?php endforeach;?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group mb-3 row">
                            <label for="job_title" class="col-sm-4 form-label align-self-center mb-lg-0 text-start">Image Produk</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="file" id="product_image" name="product_image">
                            </div>
                        </div>
                        <?php else : ?>
                                <div class="form-group mb-3 row">
                                <label for="job_title" class="col-sm-4 form-label align-self-center mb-lg-0 text-start">Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" value="" id="names" name="names">
                                </div>
                            </div>
                        <?php endif;?>

                        </div>
                    </div><!--end row-->                                                      
                </div><!--end modal-body-->
                <div class="modal-footer">          
                    <input type="hidden" name="type" value="<?=$title?>">          
                    <input type="hidden" id="id-<?=$title?>" name="id-<?=$title?>" value="">                                 
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    <button type="button" class="btn btn-dark btn-sm" data-bs-dismiss="modal">Close</button>
                </div><!--end modal-footer-->
            </form>
        </div><!--end modal-content-->
    </div><!--end modal-dialog-->
</div><!--end modal-->

<?php if($title != 'catalog'):?>
<script>
var numElement = document.querySelector('input[type="number"]')
numElement.addEventListener('change', validateMax);
numElement.addEventListener('input', validateMax);
numElement.addEventListener('keyup', validateMax);
numElement.addEventListener('paste', validateMax);

function validateMax() {
   if (this.max) this.value = Math.min(parseInt(this.max), parseInt(this.value) || 0);
}
</script>
<?php endif;?>