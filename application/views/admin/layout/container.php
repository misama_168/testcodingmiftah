<!doctype html>
<html lang="en">

<head>
<?= $meta ?>
</head>

<body>
    
<div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    <!-- Sidebar Start -->
    <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div>
        <div class="brand-logo d-flex align-items-center justify-content-between">
          <a href="./index.html" class="text-nowrap logo-img">
            <img src="<?=static_url()?>admin/assets/images/logos/favicon.png" alt="" />
            <span style="margin-left: 20px; font-size: 15px; font-weight: bolder;">Admin</span>
          </a>
          <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
            <i class="ti ti-x fs-8"></i>
          </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
          <ul id="sidebarnav">
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">Home</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="<?=base_url().'admin'?>" aria-expanded="false">
                <span>
                  <i class="ti ti-layout-dashboard"></i>
                </span>
                <span class="hide-menu">TRANSAKSI</span>
              </a>
            </li>
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">PRODUCT & CATALOG</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="<?=base_url().'admin/list/catalog'?>" aria-expanded="false">
                <span>
                  <i class="ti ti-article"></i>
                </span>
                <span class="hide-menu">Catalog</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="<?=base_url().'admin/list/product'?>" aria-expanded="false">
                <span>
                  <i class="ti ti-circle"></i>
                </span>
                <span class="hide-menu">Product</span>
              </a>
            </li>
          </ul>

        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->
    <!--  Main wrapper -->
    <div class="body-wrapper">
      <!--  Header Start -->
        <?=$header?>
      <!--  Header End -->
      <div class="container-fluid">
        <!--  Row 1 -->

        <?=$content?>
   
       
      </div>
    </div>
    <?=$footer?>
  </div>

</body>

</html>