<?php
class Upload_file
{	
	function do_upload($tipe) 
	{
		$ci = &get_instance();
		$config['upload_path']		= "../storage/".$tipe."/";
		$config['allowed_types']	= 'gif|jpg|png|jpeg|bmp|tiff';
		$config['max_size']     	= '4048'; #1MB = 1024
        $config['quality']          = '50%';
		$config['encrypt_name']		= "TRUE";
		$config['file_name']		= date('YmdHis');
	
		$ci->load->library('upload', $config);
			
		if($ci->upload->do_upload("file")):
			
			$data	 		= $ci->upload->data();
			
			#create thumbnail / resize image
			// $ci->load->library('image_lib');
			// $medium_image	= $this->resize("../storage/".$tipe."/".$data['file_name'],"../storage/".$tipe."/","253","253");
			
			$result = array('err_no'	=> "0",
							'filename'	=> $data['file_name']);
		else:
		
			$result	= array('err_no'	=> "206",
							'err_msg'	=> $ci->upload->display_errors('',''));
		
		endif;
		
		return $result;
	}
	
	function resize($file_asli,$path,$width,$height)
	{
		$ci = &get_instance();
		
		$config['image_library']	= 'gd2';
		$config['create_thumb']		= TRUE;
		$config['thumb_marker']		= "";
		$config['maintain_ratio']	= FALSE;
		$config['width'] 			= $width;
		$config['height'] 			= $height;
		$config['quality'] 			= "80%";
		$config['source_image']		= "$file_asli";
		$config['new_image']    	= "$path";
		
		$ci->image_lib->initialize($config);
		$ci->image_lib->resize();
		$ci->image_lib->clear();
		
	}
	
}
	
?>