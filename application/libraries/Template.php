<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class template {

    function load($setting = '', $konten = '') {

        $ci = &get_instance();

        $temp['meta']           = $ci->load->view('admin/layout/meta', isset($setting['meta']) ? $setting['meta'] : '', TRUE);
		$temp['header']         = $ci->load->view('admin/layout/header', isset($setting['header']) ? $setting['header'] : '' , TRUE);
        $temp['footer']         = $ci->load->view('admin/layout/footer', isset($setting['footer']) ? $setting['footer'] : '' , TRUE);
        $temp['content']        = $konten;

        /* MAIN CONTAINER */
        $ci->load->view('admin/layout/container', $temp);
    }
}
