<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function static_url()
{
    $ci =& get_instance();
    return $ci->config->item('static_path');
}

function base_url()
{
    $ci =& get_instance();
    return $ci->config->item('base_url');
}

function storage_url()
{
    $ci =& get_instance();
    return $ci->config->item('storage_path');
}

function api_url()
{
    $ci =& get_instance();
    return $ci->config->item('api_url');
}

function rp($angka){
	
	$hasil_rupiah = number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}

if (!function_exists('set_date_format')) {
    function set_date_format($date_data, $format = 'l, d F Y')
    {
      $CI = &get_instance();
      $CI->lang->load('calendar', $CI->config->item('language'));
  
      switch ($format) {
        case 'l, d F Y':
          if ($CI->lang->line('lang_code') != 'en') {
            $day = $CI->lang->line('cal_' . strtolower(date('l', strtotime($date_data))));
            $month = $CI->lang->line('cal_' . strtolower(date('F', strtotime($date_data))));
  
            $date = date('d', strtotime($date_data));
            $year = date('Y', strtotime($date_data));
  
            return "{$day}, {$date} {$month} {$year}";
          } else {
            return date($format, strtotime($date_data));
          }
          break;
        case 'd F Y':
          if ($CI->lang->line('lang_code') != 'en') {
            $month = $CI->lang->line('cal_' . strtolower(date('F', strtotime($date_data))));
  
            $date = date('d', strtotime($date_data));
            $year = date('Y', strtotime($date_data));
  
            return "{$date} {$month} {$year}";
          } else {
            return date($format, strtotime($date_data));
          }
          break;
        case 'F Y':
            if ($CI->lang->line('lang_code') != 'en') {
              $month = $CI->lang->line('cal_' . strtolower(date('F', strtotime($date_data))));
    
              $year = date('Y', strtotime($date_data));
    
              return "{$month} {$year}";
            } else {
              return date($format, strtotime($date_data));
            }
            break;
        default:
          return date('d-m-Y', strtotime($date_data));
          break;
      }
    }
}