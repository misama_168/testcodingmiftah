$(document).ready(function(){
    load_data_trx();
    load_data_list();
});

var type = $('#type').val();
var table = $('#table-'+type+'');

if(type != 'catalog'){
    var colums = [{ 'data': 'id', 'defaultContent': '', "width": 20},
    { 'data': 'catalog' },
    { 'data': 'name' },
    { 'data': 'stock'},
    { 'data': 'sold'},
    { 'data': null, "width": 100,
        createdCell: function(td, rowData) {
            var html =  '<center><a class="btn btn-outline-info btn-xs" href="#" onclick="edit('+rowData.id+',`product`)"><i class="bi bi-pencil"></i></a> <button class="btn btn-outline-danger btn-xs" onclick="deletes('+rowData.id+',`product`)"><i class="bi bi-trash font-13"></i></button></center>';
            $(td).html(html)
        }
    },
];

}else{

    var colums = [{ 'data': 'id', 'defaultContent': '', "width": 20},
    { 'data': 'names' },
    { 'data': null, "width": 100,
    createdCell: function(td, rowData) {
        var html =  '<center><a class="btn btn-outline-info btn-xs" href="#" onclick="edit('+rowData.id+',`catalog`)"><i class="bi bi-pencil"></i></a> <button class="btn btn-outline-danger btn-xs" onclick="deletes('+rowData.id+',`catalog`)"><i class="bi bi-trash font-13"></i></button></center>';
        $(td).html(html)
    }
},
];


}


function load_data_list(){
    var endpoint = base_url + 'admin/data_list?type='+type+'';
    search_tables(table, endpoint, colums);
}

var table_trx = $('#table-trx');
var colums_trx = [{ 'data': 'id', 'defaultContent': '', "width": 20},
{ 'data': 'produk' },
{ 'data': 'qty' },
{ 'data': 'created_at'},
{ 'data': 'cust'},
];

function load_data_trx(){
    var endpoint_trx = base_url + 'admin/data_trx';
    basic_tables(table_trx, endpoint_trx, colums_trx);
}

function add(val){
    if(val != 'catalog'){
        $('#modal_header_'+type+'').text('ADD PRODUCT')
        $('#name').val('')
        $('#catalog').val('')
        $('#stock').val('')
        $('#sold').val('')
        $('#img').val('')
        $('#id-'+type+'').val('')
        $('#modal_product').modal('show')
    }else{
        $('#modal_header_'+type+'').text('ADD CATALOG')
        $('#names').val('')
        $('#id-'+type+'').val('')
        $('#modal_catalog').modal('show')
    }
}


function edit(id,val){
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/data_edit?id='+id+'&type='+val+'',
        dataType: 'json',
        data: $(this).serialize(),	
        success: function (result) {
            if (result.code == 200){
    
                if(result.type != 'catalog'){
                    $('#modal_header_'+result.type+'').text('EDIT PRODUCT')
                    $('#name').val(result.data.name)
                    $('#catalog').val(result.data.catalog_id)
                    $('#stock').val(result.data.stock)
                    $('#img').val(result.data.img)
                    $('#id-'+result.type+'').val(result.data.id)
                    $('#modal_product').modal('show')

                }else{
                    $('#modal_header_'+result.type+'').text('EDIT CATALOG')
                    $('#names').val(result.data.names)
                    $('#id-'+result.type+'').val(result.data.id)
                    $('#modal_catalog').modal('show')
                }
            }else{
    
            console.log('err')          
            }
        
        },
    })
}



$("#action").validate({
    rules: {
      name: {
          required: true,
      },
      names: {
        required: true,
    },
      stock: {
        required: true,
    },
    catalog: {
        required: true,
    },
  },
  messages: {
    name: {
        required: "nama produk harus di isi",
    },   
     names: {
        required: "nama catalog harus di isi",
    },
    stock: {
      required: "stock harus di isi",
  },
  catalog: {
    required: "catalog harus di isi",
},
  },
  submitHandler: function(form) {
        
    var formData = $('#action')[0];
    var data = new FormData(formData);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: base_url + 'admin/action',
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          var obj = $.parseJSON(data);
          if (obj.n == 'SS'){
            $('#modal_'+type+'').modal('hide');

          $('#table-'+type+'').dataTable().fnDestroy();
          load_data_list();

        }else{
            $('#modal_'+type+'').modal('hide');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: obj.m,

              })   
        }
        },
        error: function (e) {    
            setTimeout(() => {
            $('.loadBtn').html("Submit").attr('disabled', false);
                Swal.fire('', 'Terjadi kesalahan pada sistem / session telah berakhir', 'warning');
            }, 1000);
            $('.loadBtn').html("Submit").attr('disabled', false); 
        }
    });

}
  });

 function deletes(id,val){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/delete_data?id='+id+'&type='+val+'',
                dataType: 'json',
                data: $(this).serialize(),	
                success: function (result) {
                    if (result.code == 200){

                        $('#table-'+type+'').dataTable().fnDestroy();
                        load_data_list();

                    }else{
            
                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: result.msg,

                    })      
                    }
                
                },
            })
        }
      })
 }

 $(function() {
  $('#daterange').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {

    $('#start_date').val(start.format('YYYY-MM-DD'));
    $('#end_date').val(end.format('YYYY-MM-DD'));

    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});

 $("#filter").validate({
  submitHandler: function (form) {
    const data = $(form).serialize();
    const params = Object.fromEntries(new URLSearchParams(data));
    console.log(params)
    if (params.type != null){
      $('#table-trx').dataTable().fnDestroy();
      
      star = params.start_date
      end = params.end_date

      var endpoint_trx = base_url + 'admin/data_trx?type='+params.type+'&start_date='+star+'&end_date='+end+'';
      basic_tables(table_trx, endpoint_trx, colums_trx);

    }else{

      console.log('err')
    }
  }
});

function basic_tables(tableTag, endpoint, colums, button = []) {
  $(tableTag).DataTable({
    ajax: {
      url: endpoint,
      type: "POST",
    },
    processing: true,
    serverSide: true,
    searching: false,
    pageLength: 0,
    lengthMenu: [
      [20, 50, 100, 200, 500, -1],
      [20, 50, 100, 200, 500, "All"],
    ],
    oLanguage: {
      sZeroRecords: "Data tidak ditemukan !",
    },
    dom: "Blrt" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-4'i><'col-sm-8'p>>",
    buttons: button,
    order: [[1, "desc"]],
    columns: colums,
    rowCallback: function (row, data, index) {
      var api = this.api();
      $("td:eq(0)", row).html(index + 1 + api.page() * api.page.len());
    },
  });
}


function search_tables(tableTag, endpoint, colums) {
    $(tableTag).DataTable({
      ajax: {
        url: endpoint,
        type: "POST",
      },
      processing: true,
      serverSide: false,
      searching: true,
      pageLength: 0,
      lengthMenu: [20, 50, 100, 200, 500],
      oLanguage: {
        sZeroRecords: "Data tidak ditemukan !",
      },
      order: [[1, "asc"]],
      columns: colums,
      rowCallback: function (row, data, index) {
        var api = this.api();
        $("td:eq(0)", row).html(index + 1 + api.page() * api.page.len());
      },
    });
  }

