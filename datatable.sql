-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 22, 2023 at 03:19 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `sembako`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `id` int NOT NULL,
  `names` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `names`) VALUES
(1, 'Konsumsi'),
(2, 'Pembersih');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `catalog_id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `stock` int NOT NULL,
  `sold` int NOT NULL DEFAULT '0',
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `catalog_id`, `name`, `stock`, `sold`, `img`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kopi', 94, 6, 'kapalapi.jpg', '2023-07-19 06:10:48', '2023-07-21 06:10:48'),
(2, 1, 'Teh', 100, 0, 'walini.jpg', '2023-07-18 06:10:48', '2023-07-21 06:10:48'),
(3, 2, 'Pasta gigi', 94, 6, 'pasta.jpg', '2023-07-22 06:16:02', '2023-07-21 06:16:02'),
(4, 2, 'Sabun Mandi', 98, 2, 'sabun.jpg', '2023-07-21 06:16:02', '2023-07-21 06:16:02'),
(5, 2, 'Sampo', 87, 13, 'sampo.png', '2023-07-20 06:16:02', '2023-07-21 06:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int NOT NULL,
  `product_id` int NOT NULL,
  `qty` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cust` varchar(50) NOT NULL DEFAULT 'miftah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `product_id`, `qty`, `created_at`, `updated_at`, `cust`) VALUES
(10, 3, 3, '2023-07-22 01:32:43', '2023-07-22 01:32:43', 'miftah'),
(11, 3, 2, '2023-07-22 01:32:54', '2023-07-22 01:32:54', 'miftah'),
(12, 3, 1, '2023-06-02 01:33:08', '2023-07-22 01:33:08', 'miftah'),
(13, 5, 10, '2023-07-22 01:34:16', '2023-07-22 01:34:16', 'miftah'),
(14, 5, 3, '2023-07-22 01:34:22', '2023-07-22 01:34:22', 'miftah'),
(15, 1, 6, '2023-07-22 01:35:58', '2023-07-22 01:35:58', 'miftah'),
(16, 4, 2, '2023-07-22 01:53:22', '2023-07-22 01:53:22', 'miftah');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;
